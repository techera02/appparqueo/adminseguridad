package app.parqueo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdminSeguridadApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdminSeguridadApplication.class, args);
	}

}
