package app.parqueo.domain.model;

public class ResponseSeleccionarPerfil extends ResponseError{

	private Perfil perfil;

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}
}
