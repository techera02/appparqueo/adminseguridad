package app.parqueo.domain.model;

public class ResponseActualizarUsuario extends ResponseError{

	private boolean resultado;

	public boolean getResultado() {
		return this.resultado;
	}

	public void setResultado(boolean resultado) {
		this.resultado = resultado;
	}
}
