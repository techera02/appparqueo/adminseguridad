package app.parqueo.domain.model;

public class ResponseSeleccionarUsuario extends ResponseError{

	private Usuario usuario;

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
