package app.parqueo.domain.model;

public class ResponseRecuperarContraseniaAdmin extends ResponseError{

	private boolean resultado;

	public boolean getResultado() {
		return resultado;
	}

	public void setResultado(boolean resultado) {
		this.resultado = resultado;
	}
}
