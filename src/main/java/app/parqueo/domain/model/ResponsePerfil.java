package app.parqueo.domain.model;

public class ResponsePerfil extends ResponsePaginado{

	private Integer perf_Id;
	private String perf_Nombre;
	private boolean perf_Activo;
	private Integer perf_UsuarioCreacion;
	private Integer perf_UsuarioEdicion;
	
	public Integer getPerf_Id() {
		return perf_Id;
	}
	public void setPerf_Id(Integer perf_Id) {
		this.perf_Id = perf_Id;
	}
	public String getPerf_Nombre() {
		return perf_Nombre;
	}
	public void setPerf_Nombre(String perf_Nombre) {
		this.perf_Nombre = perf_Nombre;
	}
	public boolean getPerf_Activo() {
		return perf_Activo;
	}
	public void setPerf_Activo(boolean perf_Activo) {
		this.perf_Activo = perf_Activo;
	}
	public Integer getPerf_UsuarioCreacion() {
		return perf_UsuarioCreacion;
	}
	public void setPerf_UsuarioCreacion(Integer perf_UsuarioCreacion) {
		this.perf_UsuarioCreacion = perf_UsuarioCreacion;
	}
	public Integer getPerf_UsuarioEdicion() {
		return perf_UsuarioEdicion;
	}
	public void setPerf_UsuarioEdicion(Integer perf_UsuarioEdicion) {
		this.perf_UsuarioEdicion = perf_UsuarioEdicion;
	}
}
