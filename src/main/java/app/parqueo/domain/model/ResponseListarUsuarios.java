package app.parqueo.domain.model;

import java.util.List;

public class ResponseListarUsuarios extends ResponseError{
	
	private List<ResponseUsuario> lista;

	public List<ResponseUsuario> getLista() {
		return lista;
	}

	public void setLista(List<ResponseUsuario> lista) {
		this.lista = lista;
	}
}
