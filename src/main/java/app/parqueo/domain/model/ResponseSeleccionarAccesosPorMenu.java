package app.parqueo.domain.model;

public class ResponseSeleccionarAccesosPorMenu extends ResponseError{

	private PerfilMenu perfilMenu;

	public PerfilMenu getPerfilMenu() {
		return perfilMenu;
	}

	public void setPerfilMenu(PerfilMenu perfilMenu) {
		this.perfilMenu = perfilMenu;
	}
}
