package app.parqueo.domain.model;

import java.util.List;

public class ResponseListarAccesosMenuPorPerfil extends ResponseError{

	private List<ResponsePerfilMenu> lista;

	public List<ResponsePerfilMenu> getLista() {
		return lista;
	}

	public void setLista(List<ResponsePerfilMenu> lista) {
		this.lista = lista;
	}
}
