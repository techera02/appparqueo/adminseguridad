package app.parqueo.domain.model;

public class ResponseUsuario extends ResponsePaginado{

	private Integer usua_Id;
	private String usua_Alias;
	private String usua_Clave;
	private String usua_Nombres;
	private String usua_Apellidos;
	private Integer perf_Id;
	private boolean usua_Activo;
	private Integer usua_UsuarioCreacion;
	private Integer usua_UsuarioEdicion;
	private String usua_Correo;
	private String perf_Nombre;
	
	public Integer getUsua_Id() {
		return usua_Id;
	}
	public void setUsua_Id(Integer usua_Id) {
		this.usua_Id = usua_Id;
	}
	public String getUsua_Alias() {
		return usua_Alias;
	}
	public void setUsua_Alias(String usua_Alias) {
		this.usua_Alias = usua_Alias;
	}
	public String getUsua_Clave() {
		return usua_Clave;
	}
	public void setUsua_Clave(String usua_Clave) {
		this.usua_Clave = usua_Clave;
	}
	public String getUsua_Nombres() {
		return usua_Nombres;
	}
	public void setUsua_Nombres(String usua_Nombres) {
		this.usua_Nombres = usua_Nombres;
	}
	public String getUsua_Apellidos() {
		return usua_Apellidos;
	}
	public void setUsua_Apellidos(String usua_Apellidos) {
		this.usua_Apellidos = usua_Apellidos;
	}
	public Integer getPerf_Id() {
		return perf_Id;
	}
	public void setPerf_Id(Integer perf_Id) {
		this.perf_Id = perf_Id;
	}
	public boolean getUsua_Activo() {
		return usua_Activo;
	}
	public void setUsua_Activo(boolean usua_Activo) {
		this.usua_Activo = usua_Activo;
	}
	public Integer getUsua_UsuarioCreacion() {
		return usua_UsuarioCreacion;
	}
	public void setUsua_UsuarioCreacion(Integer usua_UsuarioCreacion) {
		this.usua_UsuarioCreacion = usua_UsuarioCreacion;
	}
	public Integer getUsua_UsuarioEdicion() {
		return usua_UsuarioEdicion;
	}
	public void setUsua_UsuarioEdicion(Integer usua_UsuarioEdicion) {
		this.usua_UsuarioEdicion = usua_UsuarioEdicion;
	}
	public String getUsua_Correo() {
		return usua_Correo;
	}
	public void setUsua_Correo(String usua_Correo) {
		this.usua_Correo = usua_Correo;
	}
	public String getPerf_Nombre() {
		return perf_Nombre;
	}
	public void setPerf_Nombre(String perf_Nombre) {
		this.perf_Nombre = perf_Nombre;
	}
}
