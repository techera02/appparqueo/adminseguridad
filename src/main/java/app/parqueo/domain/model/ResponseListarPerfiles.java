package app.parqueo.domain.model;

import java.util.List;

public class ResponseListarPerfiles extends ResponseError{
	
	private List<ResponsePerfil> lista;

	public List<ResponsePerfil> getLista() {
		return lista;
	}

	public void setLista(List<ResponsePerfil> lista) {
		this.lista = lista;
	}
}
