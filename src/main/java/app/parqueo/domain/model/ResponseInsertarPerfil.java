package app.parqueo.domain.model;

public class ResponseInsertarPerfil extends ResponseError{

	private int Perf_Id;

	public int getPerf_Id() {
		return Perf_Id;
	}

	public void setPerf_Id(int perf_Id) {
		Perf_Id = perf_Id;
	}
}
