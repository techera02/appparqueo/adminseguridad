package app.parqueo.domain.model;

import java.util.List;

public class ResponseListarConfiguracionMenuPorPerfil extends ResponseError{

	private List<ResponsePerfilMenu2> lista;

	public List<ResponsePerfilMenu2> getLista() {
		return lista;
	}

	public void setLista(List<ResponsePerfilMenu2> lista) {
		this.lista = lista;
	}
}
