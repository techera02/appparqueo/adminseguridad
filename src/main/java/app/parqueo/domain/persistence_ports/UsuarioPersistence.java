package app.parqueo.domain.persistence_ports;

import java.util.List;

import app.parqueo.domain.model.ResponseUsuario;
import app.parqueo.domain.model.Usuario;

public interface UsuarioPersistence {
	
	Integer validarUsuarioExiste(Usuario usuario);
	
	Usuario iniciarSesionAdmin(Usuario usuario);
	
	Integer validarCorreoUsuario(Integer idusuario,String correo);
	
	Integer recuperarContraseniaAdmin(String correo,String clave);
		
	Integer insertarUsuario(Usuario usuario);
	
	Integer actualizarUsuario(Usuario usuario);
	
	Integer validarAliasUsuario(String correo);
	
	Integer validarUsuarioExistePorId(Integer idusuario);
	
	Usuario seleccionarUsuario(Integer idusuario);
	
	Integer eliminarUsuario(Integer idusuario);
	
	Usuario seleccionarUsuarioPorCorreo(String correo);
	
	List<ResponseUsuario> listarUsuarios(Integer pagenumber, Integer pagesize, String nombre, Integer idperfil, 
			Integer activo);

	Integer validarUsuarioActivo(Usuario usuario);
}
