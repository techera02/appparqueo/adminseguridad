package app.parqueo.domain.persistence_ports;

import java.util.List;

import app.parqueo.domain.model.Perfil;
import app.parqueo.domain.model.ResponsePerfil;

public interface PerfilPersistence {

	List<ResponsePerfil> listarPerfiles(Integer pagenumber, Integer pagesize, String nombre, Integer activo);
	
	Integer validarPerfil(String nombre, Integer idperfil);
	
	Integer insertarPerfil(Perfil perfil);
	
	Integer actualizarPerfil(Perfil perfil);
	
	Integer validarPerfilExiste(Integer idperfil);
	
	Perfil seleccionarPerfil(Integer idperfil);
	
	Integer eliminarPerfil(Integer idperfil);
}
