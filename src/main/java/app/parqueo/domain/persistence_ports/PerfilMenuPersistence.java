package app.parqueo.domain.persistence_ports;

import java.util.List;

import app.parqueo.domain.model.PerfilMenu;
import app.parqueo.domain.model.ResponsePerfilMenu;
import app.parqueo.domain.model.ResponsePerfilMenu2;

public interface PerfilMenuPersistence {
	
	Integer insertarPerfilMenu(PerfilMenu perfilmenu);
	
	Integer eliminarPerfilMenuPorPerfil(Integer idperfil);
	
	Integer validarAccesosPorMenu(Integer idperfil, Integer idmenu);
	
	PerfilMenu seleccionarAccesosPorMenu(Integer idperfil, Integer idmenu);
	
	List<ResponsePerfilMenu> listarAccesosMenuPorPerfil(Integer idperfil);
	
	List<ResponsePerfilMenu2> listarConfiguracionMenuPorPerfil(Integer idperfil);
	
	Integer actualizarPerfilMenu(PerfilMenu perfilmenu);
}
