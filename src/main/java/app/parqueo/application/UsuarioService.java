package app.parqueo.application;

import java.util.List;

import org.springframework.stereotype.Service;

import app.parqueo.domain.model.ResponseUsuario;
import app.parqueo.domain.model.Usuario;
import app.parqueo.domain.persistence_ports.UsuarioPersistence;

@Service
public class UsuarioService {

	UsuarioPersistence usuarioPersistence;

	public UsuarioService(UsuarioPersistence usuarioPersistence) {
		this.usuarioPersistence = usuarioPersistence;
	}

	public Integer validarUsuarioExiste(Usuario usuario) {
		return this.usuarioPersistence.validarUsuarioExiste(usuario);
	}

	public Usuario iniciarSesionAdmin(Usuario usuario) {
		return this.usuarioPersistence.iniciarSesionAdmin(usuario);
	}

	public Integer validarCorreoUsuario(Integer idusuario, String correo) {
		return this.usuarioPersistence.validarCorreoUsuario(idusuario, correo);
	}

	public Integer recuperarContraseniaAdmin(String correo, String clave) {
		return this.usuarioPersistence.recuperarContraseniaAdmin(correo, clave);
	}
	public Integer insertarUsuario(Usuario usuario) {
		return this.usuarioPersistence.insertarUsuario(usuario);
	}

	public Integer actualizarUsuario(Usuario usuario) {
		return this.usuarioPersistence.actualizarUsuario(usuario);
	}

	public Integer validarAliasUsuario(String alias) {
		return this.usuarioPersistence.validarAliasUsuario(alias);
	}

	public Integer validarUsuarioExistePorId(Integer idusuario) {
		return this.usuarioPersistence.validarUsuarioExistePorId(idusuario);
	}

	public Usuario seleccionarUsuario(Integer idusuario) {
		return this.usuarioPersistence.seleccionarUsuario(idusuario);
	}

	public Integer eliminarUsuario(Integer idusuario) {
		return this.usuarioPersistence.eliminarUsuario(idusuario);
	}

	public Usuario seleccionarUsuarioPorCorreo(String correo) {
		return this.usuarioPersistence.seleccionarUsuarioPorCorreo(correo);
	}

	public List<ResponseUsuario> listarUsuarios(Integer pagenumber, Integer pagesize, String nombre, Integer idperfil,
			Integer activo) {

		return this.usuarioPersistence.listarUsuarios(pagenumber, pagesize, nombre, idperfil, activo);
	}

	public Integer validarUsuarioActivo(Usuario usuario) {
		return this.usuarioPersistence.validarUsuarioActivo(usuario);
	}
}
