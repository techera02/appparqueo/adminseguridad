package app.parqueo.application;

import java.util.List;

import org.springframework.stereotype.Service;

import app.parqueo.domain.model.Perfil;
import app.parqueo.domain.model.ResponsePerfil;
import app.parqueo.domain.persistence_ports.PerfilPersistence;

@Service
public class PerfilService {

	PerfilPersistence perfilPersistence;
	
	public PerfilService(PerfilPersistence perfilPersistence)
	{
		this.perfilPersistence = perfilPersistence;
	}
	
	public List<ResponsePerfil> listarPerfiles(Integer pagenumber, Integer pagesize, String nombre, Integer activo)
	{
        return this.perfilPersistence.listarPerfiles(pagenumber,pagesize, nombre, activo);
    }
	
	public Integer validarPerfil(String nombre, Integer idperfil) 
	{
        return this.perfilPersistence.validarPerfil(nombre, idperfil);
    }
	
	public Integer insertarPerfil(Perfil perfil)
	{
        return this.perfilPersistence.insertarPerfil(perfil);
    }
	
	public Integer actualizarPerfil(Perfil perfil)
	{
		return this.perfilPersistence.actualizarPerfil(perfil);
	}
	
	public Integer validarPerfilExiste(Integer idperfil) {
		return this.perfilPersistence.validarPerfilExiste(idperfil);
	}
	
	public Perfil seleccionarPerfil(Integer idperfil) {
		return this.perfilPersistence.seleccionarPerfil(idperfil);
	}
	
	public Integer eliminarPerfil(Integer idperfil) {
		return this.perfilPersistence.eliminarPerfil(idperfil);
	}
}
