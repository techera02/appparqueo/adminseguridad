package app.parqueo.application;

import java.util.List;

import org.springframework.stereotype.Service;

import app.parqueo.domain.model.PerfilMenu;
import app.parqueo.domain.model.ResponsePerfilMenu;
import app.parqueo.domain.model.ResponsePerfilMenu2;
import app.parqueo.domain.persistence_ports.PerfilMenuPersistence;

@Service
public class PerfilMenuService {

	PerfilMenuPersistence perfilMenuPersistence;
	
	public PerfilMenuService(PerfilMenuPersistence perfilMenuPersistence)
	{
		this.perfilMenuPersistence = perfilMenuPersistence;
	}
	
	public Integer insertarPerfilMenu(PerfilMenu perfilmenu)
	{
        return this.perfilMenuPersistence.insertarPerfilMenu(perfilmenu);
    }
	
	public Integer eliminarPerfilMenuPorPerfil(Integer idperfil)
	{
        return this.perfilMenuPersistence.eliminarPerfilMenuPorPerfil(idperfil);
    }
	
	public Integer validarAccesosPorMenu(Integer idperfil, Integer idmenu) {
		return this.perfilMenuPersistence.validarAccesosPorMenu(idperfil,idmenu);
	}
	
	public PerfilMenu seleccionarAccesosPorMenu(Integer idperfil, Integer idmenu) {
		return this.perfilMenuPersistence.seleccionarAccesosPorMenu(idperfil,idmenu);
	}
	
	public List<ResponsePerfilMenu> listarAccesosMenuPorPerfil(Integer idperfil)
	{
        return this.perfilMenuPersistence.listarAccesosMenuPorPerfil(idperfil);
    }
	
	public List<ResponsePerfilMenu2> listarConfiguracionMenuPorPerfil(Integer idperfil)
	{
        return this.perfilMenuPersistence.listarConfiguracionMenuPorPerfil(idperfil);
    }
	
	public Integer actualizarPerfilMenu(PerfilMenu perfilmenu)
	{
        return this.perfilMenuPersistence.actualizarPerfilMenu(perfilmenu);
    }
}
