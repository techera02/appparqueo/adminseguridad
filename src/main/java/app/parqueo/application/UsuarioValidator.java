package app.parqueo.application;

import app.parqueo.domain.model.ResponseInsertarUsuario;
import app.parqueo.domain.model.Usuario;

public class UsuarioValidator {

	public ResponseInsertarUsuario InsertarUsuario(Usuario usuario)
	{
		
		ResponseInsertarUsuario response = new ResponseInsertarUsuario();
		
		response.setCodError(null);
		response.setEstado(true);
		response.setMensaje(null);
		
		if(usuario.getPerf_Id() == null) {
			response.setCodError(1);
			response.setEstado(false);
			response.setMensaje("Perfil vacio");
		}

		return response;
	}
}
