package app.parqueo.infrastructure.postgresql.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import app.parqueo.infrastructure.postgresql.entities.PerfilEntity;

public interface PerfilRepository extends JpaRepository<PerfilEntity, Integer> {
	
	@Query(value = "SELECT * FROM \"UFN_ListarPerfiles\"(:pagenumber,:pagesize,:nombre,:activo)", nativeQuery = true)
	List<Object[]> listarPerfiles(
			@Param("pagenumber") Integer pagenumber, 
			@Param("pagesize") Integer pagesize,
			@Param("nombre") String nombre,
			@Param("activo") Integer activo
			);
	
	@Query(value = "SELECT \"UFN_ValidarPerfil\"(:nombre,:idperfil)", nativeQuery = true)
	Integer validarPerfil(@Param("nombre") String nombre, @Param("idperfil") Integer idperfil);
	
	@Query(value = "SELECT * FROM \"UFN_InsertarPerfil\"(:nombre,:activo,:usuariocreacion)", nativeQuery = true)
	Integer insertarPerfil(@Param("nombre") String nombre, @Param("activo") boolean activo,
			@Param("usuariocreacion") Integer usuariocreacion);
	
	
	@Query(value = "CALL \"USP_Perfil_UPD\"(:idperfil,:nombre,:activo,:usuarioedicion)", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	@Transactional
	Integer actualizarPerfil(@Param("idperfil") Integer idperfil, @Param("nombre") String nombre,
			@Param("activo") boolean activo, @Param("usuarioedicion") Integer usuarioedicion);
	
	@Query(value = "SELECT * FROM \"UFN_ValidarPerfilExiste\"(:idperfil)", nativeQuery = true)
	Integer validarPerfilExiste(@Param("idperfil") Integer idperfil);
	
	@Query(value = "SELECT * FROM \"UFN_SeleccionarPerfil\"(:idperfil)", nativeQuery = true)
	PerfilEntity seleccionarPerfil(@Param("idperfil") Integer idperfil);
	
	@Query(value = "SELECT * FROM \"UFN_EliminarPerfil\"(:idperfil)", nativeQuery = true)
	Integer eliminarPerfil(@Param("idperfil") Integer idperfil);
}
