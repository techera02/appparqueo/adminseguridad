package app.parqueo.infrastructure.postgresql.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import app.parqueo.infrastructure.postgresql.entities.PerfilMenuEntity;

public interface PerfilMenuRepository extends JpaRepository<PerfilMenuEntity, Integer> {

	@Query(value = "CALL \"USP_PerfilMenu_INS\"(:idperfil,:idmenu,:crear,:editar,:eliminar,:activo,:usuariocreacion)", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	@Transactional
	Integer insertarPerfilMenu(@Param("idperfil") Integer idperfil, @Param("idmenu") Integer idmenu,
			@Param("crear") boolean crear, @Param("editar") boolean editar, @Param("eliminar") boolean eliminar,
			@Param("activo") boolean activo, @Param("usuariocreacion") Integer usuariocreacion);
	
	@Query(value = "SELECT * FROM \"UFN_EliminarPerfilMenuPorPerfil\"(:idperfil)", nativeQuery = true)
	Integer eliminarPerfilMenuPorPerfil(@Param("idperfil") Integer idperfil);
	
	@Query(value = "SELECT * FROM \"UFN_ValidarAccesosPorMenu\"(:idperfil,:idmenu)", nativeQuery = true)
	Integer validarAccesosPorMenu(@Param("idperfil") Integer idperfil,@Param("idmenu") Integer idmenu);
	
	@Query(value = "SELECT * FROM \"UFN_SeleccionarAccesosPorMenu\"(:idperfil,:idmenu)", nativeQuery = true)
	PerfilMenuEntity seleccionarAccesosPorMenu(@Param("idperfil") Integer idperfil,@Param("idmenu") Integer idmenu);
	
	@Query(nativeQuery = true, value = "SELECT * FROM \"UFN_ListarAccesosMenuPorPerfil\"(:idperfil)")
	List<Object[]> listarAccesosMenuPorPerfil(@Param("idperfil")Integer idperfil);
	
	@Query(nativeQuery = true, value = "SELECT * FROM \"UFN_ListarConfiguracionMenuPorPerfil\"(:idperfil)")
	List<Object[]> listarConfiguracionMenuPorPerfil(@Param("idperfil")Integer idperfil);
	
	@Query(value = "CALL \"USP_PerfilMenu_UPD\"(:idperfilmenu,:crear,:editar,:eliminar,:activo,:usuarioedicion)", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	@Transactional
	Integer actualizarPerfilMenu(@Param("idperfilmenu") Integer idperfilmenu, @Param("crear") boolean crear,
			@Param("editar") boolean editar, @Param("eliminar") boolean eliminar, @Param("activo") boolean activo,
			 @Param("usuarioedicion") Integer usuarioedicion);
}
