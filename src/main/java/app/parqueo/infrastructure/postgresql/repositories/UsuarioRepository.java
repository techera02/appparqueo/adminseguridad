package app.parqueo.infrastructure.postgresql.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import app.parqueo.infrastructure.postgresql.entities.UsuarioEntity;

public interface UsuarioRepository extends JpaRepository<UsuarioEntity, Integer> {
	
	@Query(value = "SELECT * FROM \"UFN_ValidarUsuarioExiste\"(:alias,:clave)", nativeQuery = true)
	Integer validarUsuarioExiste(@Param("alias") String alias, @Param("clave") String clave);
	
	@Query(value = "SELECT * FROM \"UFN_IniciarSesionAdmin\"(:alias,:clave)", nativeQuery = true)
	UsuarioEntity iniciarSesionAdmin(@Param("alias") String alias, @Param("clave") String clave);
	
	@Query(value = "SELECT * FROM \"UFN_ValidarCorreoUsuario\"(:idusuario,:correo)", nativeQuery = true)
	Integer validarCorreoUsuario(@Param("idusuario") Integer idusuario, @Param("correo") String correo);
	
	@Query(value = "SELECT \"UFN_RecuperarContraseniaAdmin\"(:correo,:clave)", nativeQuery = true)
	Integer recuperarContraseniaAdmin(@Param("correo") String correo, @Param("clave") String clave);
	
	/*@Query(value = "SELECT * FROM \"UFN_ListarUsuarios\"(:pagenumber,:pagesize)", nativeQuery = true)
	List<Object[]> listarUsuarios(@Param("pagenumber") Integer pagenumber, @Param("pagesize") Integer pagesize);
	*/
	
	@Query(value = "SELECT \"UFN_ValidarAliasUsuario\"(:alias)", nativeQuery = true)
	Integer validarAliasUsuario(@Param("alias") String alias);
	
	@Query(value = "CALL \"USP_Usuario_INS\"(:alias,:clave,:nombres,:apellidos,:perfil,:activo,:usuariocreacion,:correo)", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	@Transactional
	Integer insertarUsuario(
			@Param("alias") String alias, @Param("clave") String clave,
			@Param("nombres") String nombres, @Param("apellidos") String apellidos,
			@Param("perfil") Integer perfil, @Param("activo") boolean activo, @Param("usuariocreacion") Integer usuariocreacion,
			@Param("correo") String correo);
	
	
	@Query(value = "CALL \"USP_Usuario_UPD\"(:idusaurio, :alias, :nombres,:apellidos,:perfil,:activo,:usuarioedicion,:correo)", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	@Transactional
	Integer actualizarUsuario(
			@Param("idusaurio") Integer idusaurio, @Param("alias") String alias, @Param("nombres") String nombres,
			@Param("apellidos") String apellidos, @Param("perfil") Integer perfil, @Param("activo") boolean activo,
			@Param("usuarioedicion") Integer usuarioedicion, @Param("correo") String correo
			);
	
	@Query(value = "SELECT * FROM \"UFN_ValidarUsuarioExistePorId\"(:idusuario)", nativeQuery = true)
	Integer validarUsuarioExistePorId(@Param("idusuario") Integer idusuario);
	
	@Query(value = "SELECT * FROM \"UFN_SeleccionarUsuario\"(:idusuario)", nativeQuery = true)
	UsuarioEntity seleccionarUsuario(@Param("idusuario") Integer idusuario);
	
	@Query(value = "CALL \"USP_Usuario_DEL\"(:idusuario)", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	@Transactional
	Integer eliminarUsuario(@Param("idusuario") Integer idusuario);
	
	@Query(value = "SELECT * FROM \"UFN_SeleccionarUsuarioPorCorreo\"(:correo)", nativeQuery = true)
	UsuarioEntity seleccionarUsuarioPorCorreo(@Param("correo") String correo);
	
	@Query(value = "SELECT * FROM \"UFN_ListarUsuarios\"(:pagenumber,:pagesize, :nombre, :ifperfil, :activo)", nativeQuery = true)
	List<Object[]> listarUsuarios(@Param("pagenumber") Integer pagenumber, @Param("pagesize") Integer pagesize,
			@Param("nombre") String nombre, @Param("ifperfil") Integer ifperfil, @Param("activo") Integer activo);
	
	@Query(value = "SELECT * FROM \"UFN_ValidarUsuarioActivo\"(:alias,:clave)", nativeQuery = true)
	Integer validarUsuarioActivo(@Param("alias") String alias, @Param("clave") String clave);
	
}
