package app.parqueo.infrastructure.postgresql.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import app.parqueo.domain.model.PerfilMenu;

@Entity
@Table(name="\"PerfilMenu\"")
public class PerfilMenuEntity {	
	@Id
	private Integer Pmen_Id;
	private Integer Perf_Id;
	private Integer Menu_Id;
	private boolean Pmen_Crear;
	private boolean Pmen_Editar;
	private boolean Pmen_Eliminar;
	private boolean Pmen_Activo;
	
	public Integer getPmen_Id() {
		return Pmen_Id;
	}
	public void setPmen_Id(Integer pmen_Id) {
		Pmen_Id = pmen_Id;
	}
	public Integer getPerf_Id() {
		return Perf_Id;
	}
	public void setPerf_Id(Integer perf_Id) {
		Perf_Id = perf_Id;
	}
	public Integer getMenu_Id() {
		return Menu_Id;
	}
	public void setMenu_Id(Integer menu_Id) {
		Menu_Id = menu_Id;
	}
	public boolean getPmen_Crear() {
		return Pmen_Crear;
	}
	public void setPmen_Crear(boolean pmen_Crear) {
		Pmen_Crear = pmen_Crear;
	}
	public boolean getPmen_Editar() {
		return Pmen_Editar;
	}
	public void setPmen_Editar(boolean pmen_Editar) {
		Pmen_Editar = pmen_Editar;
	}
	public boolean getPmen_Eliminar() {
		return Pmen_Eliminar;
	}
	public void setPmen_Eliminar(boolean pmen_Eliminar) {
		Pmen_Eliminar = pmen_Eliminar;
	}
	
	public boolean isPmen_Activo() {
		return Pmen_Activo;
	}
	public void setPmen_Activo(boolean pmen_Activo) {
		Pmen_Activo = pmen_Activo;
	}
	
	public PerfilMenu toPerfilMenu() {
		PerfilMenu perfilMenu = new PerfilMenu();
        BeanUtils.copyProperties(this, perfilMenu);
        return perfilMenu;
    }
}
