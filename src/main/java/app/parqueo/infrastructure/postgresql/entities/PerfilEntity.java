package app.parqueo.infrastructure.postgresql.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import app.parqueo.domain.model.Perfil;

@Entity
@Table(name="\"Perfil\"")
public class PerfilEntity {
	@Id
	private Integer Perf_Id;
	private String Perf_Nombre;
	private boolean Perf_Activo;
	private Integer Perf_UsuarioCreacion;
	private Integer Perf_UsuarioEdicion;
	
	public Integer getPerf_Id() {
		return Perf_Id;
	}
	public void setPerf_Id(Integer perf_Id) {
		Perf_Id = perf_Id;
	}
	public String getPerf_Nombre() {
		return Perf_Nombre;
	}
	public void setPerf_Nombre(String perf_Nombre) {
		Perf_Nombre = perf_Nombre;
	}
	public boolean getPerf_Activo() {
		return Perf_Activo;
	}
	public void setPerf_Activo(boolean perf_Activo) {
		Perf_Activo = perf_Activo;
	}
	public Integer getPerf_UsuarioCreacion() {
		return Perf_UsuarioCreacion;
	}
	public void setPerf_UsuarioCreacion(Integer perf_UsuarioCreacion) {
		Perf_UsuarioCreacion = perf_UsuarioCreacion;
	}
	public Integer getPerf_UsuarioEdicion() {
		return Perf_UsuarioEdicion;
	}
	public void setPerf_UsuarioEdicion(Integer perf_UsuarioEdicion) {
		Perf_UsuarioEdicion = perf_UsuarioEdicion;
	}
	
	public Perfil toPerfil() {
		Perfil perfil = new Perfil();
        BeanUtils.copyProperties(this, perfil);
        return perfil;
    }
}
