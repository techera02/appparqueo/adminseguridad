package app.parqueo.infrastructure.postgresql.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import app.parqueo.domain.model.Usuario;

@Entity
@Table(name="\"Usuario\"")
public class UsuarioEntity {
	@Id
	private Integer Usua_Id;
	private String Usua_Alias;
	private String Usua_Clave;
	private String Usua_Nombres;
	private String Usua_Apellidos;
	private Integer Perf_Id;
	private boolean Usua_Activo;
	private Integer Usua_UsuarioCreacion;
	private Integer Usua_UsuarioEdicion;
	private String Usua_Correo;
	
	public Integer getUsua_Id() {
		return Usua_Id;
	}
	public void setUsua_Id(Integer usua_Id) {
		Usua_Id = usua_Id;
	}
	public String getUsua_Alias() {
		return Usua_Alias;
	}
	public void setUsua_Alias(String usua_Alias) {
		Usua_Alias = usua_Alias;
	}
	public String getUsua_Clave() {
		return Usua_Clave;
	}
	public void setUsua_Clave(String usua_Clave) {
		Usua_Clave = usua_Clave;
	}
	public String getUsua_Nombres() {
		return Usua_Nombres;
	}
	public void setUsua_Nombres(String usua_Nombres) {
		Usua_Nombres = usua_Nombres;
	}
	public String getUsua_Apellidos() {
		return Usua_Apellidos;
	}
	public void setUsua_Apellidos(String usua_Apellidos) {
		Usua_Apellidos = usua_Apellidos;
	}
	public Integer getPerf_Id() {
		return Perf_Id;
	}
	public void setPerf_Id(Integer perf_Id) {
		Perf_Id = perf_Id;
	}
	public boolean getUsua_Activo() {
		return Usua_Activo;
	}
	public void setUsua_Activo(boolean usua_Activo) {
		Usua_Activo = usua_Activo;
	}	
	
	public Integer getUsua_UsuarioCreacion() {
		return Usua_UsuarioCreacion;
	}
	public void setUsua_UsuarioCreacion(Integer usua_UsuarioCreacion) {
		Usua_UsuarioCreacion = usua_UsuarioCreacion;
	}
	public Integer getUsua_UsuarioEdicion() {
		return Usua_UsuarioEdicion;
	}
	public void setUsua_UsuarioEdicion(Integer usua_UsuarioEdicion) {
		Usua_UsuarioEdicion = usua_UsuarioEdicion;
	}	
	
	public String getUsua_Correo() {
		return Usua_Correo;
	}
	public void setUsua_Correo(String usua_Correo) {
		Usua_Correo = usua_Correo;
	}
	
	public Usuario toUsuario() {
		Usuario usuario = new Usuario();
        BeanUtils.copyProperties(this, usuario);
        return usuario;
    }
}
