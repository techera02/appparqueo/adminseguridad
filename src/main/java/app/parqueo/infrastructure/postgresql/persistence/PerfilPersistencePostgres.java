package app.parqueo.infrastructure.postgresql.persistence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.parqueo.domain.model.Perfil;
import app.parqueo.domain.model.ResponsePerfil;
import app.parqueo.domain.persistence_ports.PerfilPersistence;
import app.parqueo.infrastructure.postgresql.repositories.PerfilRepository;

@Repository("PerfilPersistence")
public class PerfilPersistencePostgres implements PerfilPersistence{

	private final PerfilRepository perfilRepository;
	
	@Autowired
    public PerfilPersistencePostgres(PerfilRepository perfilRepository) 
    {
        this.perfilRepository = perfilRepository;
    }
	
	@Override
	public List<ResponsePerfil> listarPerfiles(Integer pagenumber, Integer pagesize, String nombre, Integer activo) {
		List<Object[]> list = this.perfilRepository.listarPerfiles(pagenumber,pagesize,nombre,activo);
		List<ResponsePerfil> listaPerfil = new ArrayList<ResponsePerfil>();
		
		for(int x = 0;x<list.size();x++) {
			ResponsePerfil entidad = new ResponsePerfil();
			entidad.setRowNumber(Integer.parseInt(list.get(x)[0].toString()));
			entidad.setRowCount(Integer.parseInt(list.get(x)[1].toString()));
			entidad.setPageCount(Integer.parseInt(list.get(x)[2].toString()));
			entidad.setPageIndex(Integer.parseInt(list.get(x)[3].toString()));			
			entidad.setPerf_Id(Integer.parseInt(list.get(x)[4].toString()));
			entidad.setPerf_Nombre(list.get(x)[5].toString());
			entidad.setPerf_Activo(Boolean.parseBoolean(list.get(x)[6].toString()));			
			entidad.setPerf_UsuarioCreacion(Integer.parseInt(list.get(x)[7].toString()));
			entidad.setPerf_UsuarioEdicion(Integer.parseInt(list.get(x)[8].toString()));
			
			listaPerfil.add(entidad);
		}
		return listaPerfil;
	}
	
	@Override
	public Integer validarPerfil(String nombre, Integer idPerfil) 
	{
		return this.perfilRepository.validarPerfil(nombre, idPerfil);
	}
	
	@Override
	public Integer insertarPerfil(Perfil perfil) 
	{
		return this.perfilRepository.insertarPerfil(perfil.getPerf_Nombre(), perfil.getPerf_Activo(),
				perfil.getPerf_UsuarioCreacion());
	}
	
	@Override
	public Integer actualizarPerfil(Perfil perfil) {
		return this.perfilRepository.actualizarPerfil(perfil.getPerf_Id(), perfil.getPerf_Nombre(),
				perfil.getPerf_Activo(), perfil.getPerf_UsuarioEdicion());
	}
	
	@Override
	public Integer validarPerfilExiste(Integer idperfil) {
		return this.perfilRepository.validarPerfilExiste(idperfil);
	}
	
	@Override
	public Perfil seleccionarPerfil(Integer idperfil) {
		return this.perfilRepository.seleccionarPerfil(idperfil).toPerfil();
	}
	
	@Override
	public Integer eliminarPerfil(Integer idperfil) {
		return this.perfilRepository.eliminarPerfil(idperfil);
	}
}
