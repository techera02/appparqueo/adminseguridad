package app.parqueo.infrastructure.postgresql.persistence;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import app.parqueo.domain.model.PerfilMenu;
import app.parqueo.domain.model.ResponsePerfilMenu;
import app.parqueo.domain.model.ResponsePerfilMenu2;
import app.parqueo.domain.persistence_ports.PerfilMenuPersistence;
import app.parqueo.infrastructure.postgresql.repositories.PerfilMenuRepository;

@Repository("PerfilMenuPersistence")
public class PerfilMenuPersistencePostgres implements PerfilMenuPersistence{

	private final PerfilMenuRepository perfilMenuRepository;
	
	@Autowired
    public PerfilMenuPersistencePostgres(PerfilMenuRepository perfilMenuRepository) 
    {
        this.perfilMenuRepository = perfilMenuRepository;
    }
	
	@Override
	public Integer insertarPerfilMenu(PerfilMenu perfilmenu)
	{
		return this.perfilMenuRepository.insertarPerfilMenu(perfilmenu.getPerf_Id(), perfilmenu.getMenu_Id(),
				perfilmenu.getPmen_Crear(), perfilmenu.getPmen_Editar(), perfilmenu.getPmen_Eliminar(), 
				perfilmenu.getPmen_Activo(), perfilmenu.getPmen_UsuarioCreacion());
	}
	
	@Override
	public Integer eliminarPerfilMenuPorPerfil(Integer idperfil) 
	{
		return this.perfilMenuRepository.eliminarPerfilMenuPorPerfil(idperfil);
	}
	
	@Override
	public Integer validarAccesosPorMenu(Integer idperfil, Integer idmenu) {
		return this.perfilMenuRepository.validarAccesosPorMenu(idperfil,idmenu);
	}
	
	@Override
	public PerfilMenu seleccionarAccesosPorMenu(Integer idperfil, Integer idmenu) {
		return this.perfilMenuRepository.seleccionarAccesosPorMenu(idperfil,idmenu).toPerfilMenu();
	}
	
	@Override
	public List<ResponsePerfilMenu> listarAccesosMenuPorPerfil(Integer idperfil) {
		List<ResponsePerfilMenu> listaPerfilMenu = new ArrayList<ResponsePerfilMenu>();
		
		List<Object[]> list = this.perfilMenuRepository.listarAccesosMenuPorPerfil(idperfil);
		
		for(int i = 0; i<list.size();i++) {
			ResponsePerfilMenu entidad = new ResponsePerfilMenu();
			entidad.setPmen_Id(Integer.parseInt(list.get(i)[0].toString()));
			entidad.setPerf_Id(Integer.parseInt(list.get(i)[1].toString()));
			entidad.setMenu_Id(Integer.parseInt(list.get(i)[2].toString()));
			
			if(list.get(i)[3] != null) {
				entidad.setMenu_PadreId(Integer.parseInt(list.get(i)[3].toString()));
	        }else {
	        	entidad.setMenu_PadreId(null);
	        }
			
			entidad.setMenu_Nombre(list.get(i)[4].toString());
			entidad.setMenu_Url(list.get(i)[5].toString());
			entidad.setMenu_Posicion(Integer.parseInt(list.get(i)[6].toString()));			
			entidad.setPmen_Crear(Boolean.parseBoolean(list.get(i)[7].toString()));
			entidad.setPmen_Editar(Boolean.parseBoolean(list.get(i)[8].toString()));
			entidad.setPmen_Eliminar(Boolean.parseBoolean(list.get(i)[9].toString()));
			entidad.setPmen_Activo(Boolean.parseBoolean(list.get(i)[10].toString()));
	        
			listaPerfilMenu.add(entidad);
		}

		return listaPerfilMenu;		
	}
	
	@Override
	public List<ResponsePerfilMenu2> listarConfiguracionMenuPorPerfil(Integer idperfil) {
		List<ResponsePerfilMenu2> listaPerfilMenu = new ArrayList<ResponsePerfilMenu2>();
		
		List<Object[]> list = this.perfilMenuRepository.listarConfiguracionMenuPorPerfil(idperfil);
		
		for(int i = 0; i<list.size();i++) {
			ResponsePerfilMenu2 entidad = new ResponsePerfilMenu2();
			entidad.setNivel(Integer.parseInt(list.get(i)[0].toString()));
			
			if(list.get(i)[1] != null) {
				entidad.setPerf_Id(Integer.parseInt(list.get(i)[1].toString()));
	        }else {
	        	entidad.setPerf_Id(null);
	        }
			
			entidad.setMenu_Id(Integer.parseInt(list.get(i)[2].toString()));
			
			if(list.get(i)[3] != null) {
				entidad.setMenu_PadreId(Integer.parseInt(list.get(i)[3].toString()));
	        }else {
	        	entidad.setMenu_PadreId(null);
	        }
			
			entidad.setMenu_Nombre(list.get(i)[4].toString());		
			entidad.setPmen_Crear(Boolean.parseBoolean(list.get(i)[5].toString()));
			entidad.setPmen_Editar(Boolean.parseBoolean(list.get(i)[6].toString()));
			entidad.setPmen_Eliminar(Boolean.parseBoolean(list.get(i)[7].toString()));
			entidad.setPmen_Activo(Boolean.parseBoolean(list.get(i)[8].toString()));
			
			if(list.get(i)[9] != null) {
				entidad.setPmen_Id(Integer.parseInt(list.get(i)[9].toString()));
	        }else {
	        	entidad.setPmen_Id(null);
	        }
	        
			listaPerfilMenu.add(entidad);
		}

		return listaPerfilMenu;		
	}
	
	@Override
	public Integer actualizarPerfilMenu(PerfilMenu perfilmenu)
	{
		return this.perfilMenuRepository.actualizarPerfilMenu(perfilmenu.getPmen_Id(), perfilmenu.getPmen_Crear(),
				perfilmenu.getPmen_Editar(), perfilmenu.getPmen_Eliminar(), perfilmenu.getPmen_Activo(),
				perfilmenu.getPmen_UsuarioEdicion());
	}
}
