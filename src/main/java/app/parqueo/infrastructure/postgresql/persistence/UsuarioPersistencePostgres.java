package app.parqueo.infrastructure.postgresql.persistence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.parqueo.domain.model.ResponseUsuario;
import app.parqueo.domain.model.Usuario;
import app.parqueo.domain.persistence_ports.UsuarioPersistence;
import app.parqueo.infrastructure.postgresql.repositories.UsuarioRepository;

@Repository("UsuarioPersistence")
public class UsuarioPersistencePostgres implements UsuarioPersistence {
	
	private final UsuarioRepository usuarioRepository;
	
	@Autowired
    public UsuarioPersistencePostgres(UsuarioRepository usuarioRepository) 
    {
        this.usuarioRepository = usuarioRepository;
    }
	
	@Override
	public Integer validarUsuarioExiste(Usuario usuario) {
		return this.usuarioRepository.validarUsuarioExiste(usuario.getUsua_Alias(),usuario.getUsua_Clave());
	}
	
	@Override
	public Usuario iniciarSesionAdmin(Usuario usuario) {
		return this.usuarioRepository.iniciarSesionAdmin(usuario.getUsua_Alias(),usuario.getUsua_Clave()).toUsuario();
	}
	
	@Override
	public Integer validarCorreoUsuario(Integer idusuario, String correo) {
		return this.usuarioRepository.validarCorreoUsuario(idusuario,correo);
	}
	
	@Override
	public Integer recuperarContraseniaAdmin(String correo,String clave) {
		return this.usuarioRepository.recuperarContraseniaAdmin(correo,clave);
	}
	
	@Override
	public Integer insertarUsuario(Usuario usuario) 
	{
		return this.usuarioRepository.insertarUsuario(usuario.getUsua_Alias(), usuario.getUsua_Clave(),
				usuario.getUsua_Nombres(), usuario.getUsua_Apellidos(), usuario.getPerf_Id(), usuario.getUsua_Activo(),
				usuario.getUsua_UsuarioCreacion(), usuario.getUsua_Correo());
	}


	@Override
	public Integer validarAliasUsuario(String alias) 
	{
		return this.usuarioRepository.validarAliasUsuario(alias);
	}


	@Override
	public Integer actualizarUsuario(Usuario usuario) {
		return this.usuarioRepository.actualizarUsuario(usuario.getUsua_Id(), usuario.getUsua_Alias(), usuario.getUsua_Nombres()
				, usuario.getUsua_Apellidos(), usuario.getPerf_Id(), usuario.getUsua_Activo(), usuario.getUsua_UsuarioEdicion()
				, usuario.getUsua_Correo());
	}
	
	@Override
	public Integer validarUsuarioExistePorId(Integer idusuario) {
		return this.usuarioRepository.validarUsuarioExistePorId(idusuario);
	}
	
	@Override
	public Usuario seleccionarUsuario(Integer idusuario) {
		return this.usuarioRepository.seleccionarUsuario(idusuario).toUsuario();
	}
	
	@Override
	public Integer eliminarUsuario(Integer idusuario) {
		return this.usuarioRepository.eliminarUsuario(idusuario);
	}
	
	@Override
	public Usuario seleccionarUsuarioPorCorreo(String correo) {
		return this.usuarioRepository.seleccionarUsuarioPorCorreo(correo).toUsuario();
	}

	@Override
	public List<ResponseUsuario> listarUsuarios(Integer pagenumber, Integer pagesize, String nombre,
			Integer idperfil, Integer activo) {
		List<Object[]> list = this.usuarioRepository.listarUsuarios(pagenumber, pagesize, nombre, idperfil, activo);
		List<ResponseUsuario> listaUsuario = new ArrayList<ResponseUsuario>();
		
		for(int x = 0;x<list.size();x++) {
			ResponseUsuario entidad = new ResponseUsuario();
			entidad.setRowNumber(Integer.parseInt(list.get(x)[0].toString()));
			entidad.setRowCount(Integer.parseInt(list.get(x)[1].toString()));
			entidad.setPageCount(Integer.parseInt(list.get(x)[2].toString()));
			entidad.setPageIndex(Integer.parseInt(list.get(x)[3].toString()));			
			entidad.setUsua_Id(Integer.parseInt(list.get(x)[4].toString()));
			entidad.setUsua_Alias(list.get(x)[5].toString());
			entidad.setUsua_Clave(list.get(x)[6].toString());			
			entidad.setUsua_Nombres(list.get(x)[7].toString());
			entidad.setUsua_Apellidos(list.get(x)[8].toString());
			entidad.setPerf_Id(Integer.parseInt(list.get(x)[9].toString()));
			entidad.setUsua_Activo(Boolean.parseBoolean(list.get(x)[10].toString()));			
			entidad.setUsua_UsuarioCreacion(Integer.parseInt(list.get(x)[11].toString()));
			entidad.setUsua_UsuarioEdicion(Integer.parseInt(list.get(x)[12].toString()));
			entidad.setUsua_Correo(list.get(x)[13].toString());
			entidad.setPerf_Nombre(list.get(x)[14].toString());
			
			listaUsuario.add(entidad);
		}
		return listaUsuario;
	}

	@Override
	public Integer validarUsuarioActivo(Usuario usuario) {
		return this.usuarioRepository.validarUsuarioActivo(usuario.getUsua_Alias(), usuario.getUsua_Clave());
	}
}
