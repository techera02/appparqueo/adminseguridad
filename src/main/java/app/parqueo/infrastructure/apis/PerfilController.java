package app.parqueo.infrastructure.apis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.parqueo.application.PerfilService;
import app.parqueo.domain.model.Perfil;
import app.parqueo.domain.model.ResponseActualizarPerfil;
import app.parqueo.domain.model.ResponseEliminarPerfil;
import app.parqueo.domain.model.ResponseInsertarPerfil;
import app.parqueo.domain.model.ResponseListarPerfiles;
import app.parqueo.domain.model.ResponseSeleccionarPerfil;

@RestController
@RequestMapping("/Perfil")
public class PerfilController {

	private final PerfilService perfilService;
	
	@Autowired
    public PerfilController(PerfilService perfilService) 
	{
        this.perfilService = perfilService;
    }
	
	@GetMapping("/ListarPerfiles/{pagenumber}/{pagesize}/{nombre}/{activo}/{indiceNombre}")
    public ResponseEntity<ResponseListarPerfiles> listarPerfiles(
    		@PathVariable("pagenumber")Integer pagenumber, @PathVariable("pagesize")Integer pagesize,
    		@PathVariable("nombre")String nombre,  @PathVariable("activo")Integer activo,
    		@PathVariable("indiceNombre")Integer indiceNombre)
	{  
		ResponseListarPerfiles response = new ResponseListarPerfiles();
		response.setEstado(true);
		
		if(indiceNombre==0) {
			nombre = "";
		}
		
			try {
				response.setLista(perfilService.listarPerfiles(pagenumber,pagesize,nombre,activo));
			}catch(Exception e) {
				response.setCodError(0);
				response.setEstado(false);
				response.setMensaje(e.getMessage());
			}
		
		return ResponseEntity.ok(response);
    }
	
	@PostMapping("/InsertarPerfil")
    public ResponseEntity<ResponseInsertarPerfil> insertarPerfil
    (@RequestBody Perfil perfil)
	{  
		ResponseInsertarPerfil response = new ResponseInsertarPerfil();
		response.setEstado(true);
		
		String nombre = perfil.getPerf_Nombre();
		perfil.setPerf_Nombre(formatoNombre(nombre));
		
		try {
			
			Integer temp = this.perfilService.validarPerfil(perfil.getPerf_Nombre(),0);
			
			if(temp == 0) {
				Integer indice = this.perfilService.insertarPerfil(perfil);
				response.setPerf_Id(indice);
					
			}else {
				response.setCodError(1);
				response.setMensaje("El perfil ya existe");
				response.setEstado(false);
			}
				
		}catch(Exception e) {
				response.setCodError(0);
				response.setMensaje("Error no controlado");
				response.setEstado(false);
			}
		
		return ResponseEntity.ok(response);
	}
	
	@PutMapping("/ActualizarPerfil")
	public ResponseEntity<ResponseActualizarPerfil> actualizarPerfil
	(@RequestBody Perfil perfil)
	{ 
		ResponseActualizarPerfil response = new ResponseActualizarPerfil();
		response.setEstado(true);
		
		String nombre = perfil.getPerf_Nombre();
		perfil.setPerf_Nombre(formatoNombre(nombre));
		try {
			
			Integer temp = this.perfilService.validarPerfil(perfil.getPerf_Nombre(), perfil.getPerf_Id());
			
			if(temp == 0) {
				this.perfilService.actualizarPerfil(perfil);
				response.setPerf_Id(perfil.getPerf_Id());
					
			}else {
				response.setCodError(1);
				response.setMensaje("El perfil ya existe");
				response.setEstado(false);
			}
			
		}catch(Exception e) {
			response.setCodError(0);
			response.setMensaje("Error no controlador");
			response.setEstado(false);
			
		}
		
		return ResponseEntity.ok(response);
	}
	
	@GetMapping("/SeleccionarPerfil/{idperfil}")
	public ResponseEntity<ResponseSeleccionarPerfil> seleccionarPerfil(@PathVariable("idperfil")Integer idperfil) {
		ResponseSeleccionarPerfil response = new ResponseSeleccionarPerfil();
		response.setEstado(true);
		
		try {
			
			Integer temp = this.perfilService.validarPerfilExiste(idperfil);
			
			if(temp == 1) {
				response.setPerfil(this.perfilService.seleccionarPerfil(idperfil));
			}else {
				response.setCodError(1);
				response.setMensaje("Perfil no registrado");
				response.setEstado(false);
			}
			
			
		}catch(Exception e ) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
		}
		return ResponseEntity.ok(response);
	}
	
	@DeleteMapping("/EliminarPerfil/{idperfil}")
	public ResponseEntity<ResponseEliminarPerfil> eliminarPerfil(@PathVariable("idperfil")Integer idperfil) {
		ResponseEliminarPerfil response = new ResponseEliminarPerfil();
		response.setEstado(true);
		
		try {
			
			Integer temp = this.perfilService.validarPerfilExiste(idperfil);
			
			if(temp == 1) {
				
				Integer temp1 = this.perfilService.eliminarPerfil(idperfil);
				
				if(temp1 == 1) {
					response.setResultado(true);
				}else if(temp1 == -1) {
					response.setCodError(2);
					response.setMensaje("El perfil tiene dependencias");
					response.setEstado(false);
				}else {
					response.setCodError(3);
					response.setMensaje("No se pudo eliminar el perfil");
					response.setEstado(false);
				}
				
			}else {
				response.setCodError(1);
				response.setMensaje("Perfil no registrado");
				response.setEstado(false);
			}
			
			
		}catch(Exception e ) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
		}
		return ResponseEntity.ok(response);
	}
	
	public String formatoNombre(String dato) {
		dato = dato.toLowerCase();
		char[] arr = dato.trim().toCharArray();
		
		for(int x=0;x<dato.length();x++) {
			if(x==0) {
				arr[x] = Character.toUpperCase(arr[x]);
			}
			
			if(arr[x] == ' ') {
				if(x+1 < dato.length()) {
					if(arr[x+1] != ' ') {
						arr[x+1] = Character.toUpperCase(arr[x+1]);
					}
				}
			}
		}
		return new String(arr);
	}
}
