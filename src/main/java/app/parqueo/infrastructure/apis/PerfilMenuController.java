package app.parqueo.infrastructure.apis;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.parqueo.application.PerfilMenuService;
import app.parqueo.application.PerfilService;
import app.parqueo.domain.model.PerfilMenu;
import app.parqueo.domain.model.ResponseInsertarConfiguracionPerfil;
import app.parqueo.domain.model.ResponseListarAccesosMenuPorPerfil;
import app.parqueo.domain.model.ResponseListarConfiguracionMenuPorPerfil;
import app.parqueo.domain.model.ResponseSeleccionarAccesosPorMenu;

@RestController
@RequestMapping("/PerfilMenu")
public class PerfilMenuController {

	private final PerfilMenuService perfilMenuService;
	private final PerfilService perfilService;

	@Autowired
	public PerfilMenuController(PerfilMenuService perfilMenuService, PerfilService perfilService) {
		this.perfilMenuService = perfilMenuService;
		this.perfilService = perfilService;
	}

	@PostMapping("/InsertarConfiguracionPerfil")
	public ResponseEntity<ResponseInsertarConfiguracionPerfil> insertarConfiguracionPerfil(
			@RequestBody List<PerfilMenu> lista) {
		ResponseInsertarConfiguracionPerfil response = new ResponseInsertarConfiguracionPerfil();
		response.setEstado(true);
		response.setResultado(true);

		try {

			for (int x = 0; x < lista.size(); x++) {
				
				if(lista.get(x).getPmen_Id() == 0) {
					this.perfilMenuService.insertarPerfilMenu(lista.get(x));
				}else {
					this.perfilMenuService.actualizarPerfilMenu(lista.get(x));
				}
			}

		} catch (Exception e) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
			response.setResultado(false);
		}

		return ResponseEntity.ok(response);
	}

	@GetMapping("/SeleccionarAccesosPorMenu/{idperfil}/{idmenu}")
	public ResponseEntity<ResponseSeleccionarAccesosPorMenu> seleccionarAccesosPorMenu(
			@PathVariable("idperfil") Integer idperfil, @PathVariable("idmenu") Integer idmenu) {
		ResponseSeleccionarAccesosPorMenu response = new ResponseSeleccionarAccesosPorMenu();
		response.setEstado(true);

		try {

			Integer temp = this.perfilMenuService.validarAccesosPorMenu(idperfil, idmenu);

			if (temp == 1) {
				response.setPerfilMenu(this.perfilMenuService.seleccionarAccesosPorMenu(idperfil, idmenu));
			} else {
				response.setCodError(1);
				response.setMensaje("PerfilMenu no registrado");
				response.setEstado(false);
			}

		} catch (Exception e) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
		}
		return ResponseEntity.ok(response);
	}

	@GetMapping("/ListarAccesosMenuPorPerfil/{idperfil}")
	public ResponseEntity<ResponseListarAccesosMenuPorPerfil> listarAccesosMenuPorPerfil(
			@PathVariable("idperfil") Integer idperfil) {

		ResponseListarAccesosMenuPorPerfil response = new ResponseListarAccesosMenuPorPerfil();
		response.setEstado(true);

		Integer temp = this.perfilService.validarPerfilExiste(idperfil);

		if (temp == 1) {
			try {
				response.setLista(this.perfilMenuService.listarAccesosMenuPorPerfil(idperfil));

			} catch (Exception e) {
				response.setCodError(0);
				response.setEstado(false);
				response.setMensaje("Error no controlado");
			}

		} else {
			response.setCodError(1);
			response.setEstado(false);
			response.setMensaje("Perfil no registrado");
		}

		return ResponseEntity.ok(response);
	}

	@GetMapping("/ListarConfiguracionMenuPorPerfil/{idperfil}")
	public ResponseEntity<ResponseListarConfiguracionMenuPorPerfil> listarConfiguracionMenuPorPerfil(
			@PathVariable("idperfil") Integer idperfil) {

		ResponseListarConfiguracionMenuPorPerfil response = new ResponseListarConfiguracionMenuPorPerfil();
		response.setEstado(true);

		try {
			response.setLista(this.perfilMenuService.listarConfiguracionMenuPorPerfil(idperfil));

		} catch (Exception e) {
			response.setCodError(0);
			response.setEstado(false);
			response.setMensaje("Error no controlado");
		}

		return ResponseEntity.ok(response);
	}
}
