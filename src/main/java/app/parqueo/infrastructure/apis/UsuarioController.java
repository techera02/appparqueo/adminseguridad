package app.parqueo.infrastructure.apis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.context.Context;

import app.parqueo.application.EmailService;
import app.parqueo.application.UsuarioService;
import app.parqueo.application.UsuarioValidator;
import app.parqueo.domain.model.GenerarPassword;
import app.parqueo.domain.model.ResponseActualizarUsuario;
import app.parqueo.domain.model.ResponseEliminarUsuario;
import app.parqueo.domain.model.ResponseIniciarSesionAdmin;
import app.parqueo.domain.model.ResponseInsertarUsuario;
import app.parqueo.domain.model.ResponseListarUsuarios;
import app.parqueo.domain.model.ResponseRecuperarContraseniaAdmin;
import app.parqueo.domain.model.ResponseSeleccionarUsuario;
import app.parqueo.domain.model.Usuario;

@RestController
@RequestMapping("/Usuario")
public class UsuarioController {

	private final UsuarioService usuarioService;

	@Autowired
	public UsuarioController(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	@PostMapping("/IniciarSesionAdmin")
	public ResponseEntity<ResponseIniciarSesionAdmin> iniciarSesionAdmin(@RequestBody Usuario usuario) {
		ResponseIniciarSesionAdmin response = new ResponseIniciarSesionAdmin();
		response.setEstado(true);

		try {

			Integer temp = this.usuarioService.validarUsuarioExiste(usuario);
			if (temp == 1) {

				Integer temp2 = this.usuarioService.validarUsuarioActivo(usuario);
				if (temp2 == 1) {
					response.setUsuario(usuarioService.iniciarSesionAdmin(usuario));
				} else {
					response.setCodError(2);
					response.setMensaje(
							"El usuario " + usuario.getUsua_Alias() + " no cuenta con permiso para acceder al sistema");
					response.setEstado(false);
				}
			} else {
				response.setCodError(1);
				response.setMensaje("Usuario o contraseña inválidos");
				response.setEstado(false);
			}

		} catch (Exception e) {
			response.setCodError(0);
			response.setEstado(false);
			response.setMensaje(e.getMessage());
		}

		return ResponseEntity.ok(response);
	}

	@PostMapping("/RecuperarContraseniaAdmin")
	public ResponseEntity<ResponseRecuperarContraseniaAdmin> recuperarContraseniaAdmin(@RequestBody Usuario usuario) {
		ResponseRecuperarContraseniaAdmin response = new ResponseRecuperarContraseniaAdmin();

		try {
			Integer temp = this.usuarioService.validarCorreoUsuario(0, usuario.getUsua_Correo());

			if (temp == 1) {
				String clave = GenerarPassword.getRandomPassword(8);
				this.usuarioService.recuperarContraseniaAdmin(usuario.getUsua_Correo(), clave);

				Usuario usuarioBE = this.usuarioService.seleccionarUsuarioPorCorreo(usuario.getUsua_Correo());

				// variables
				Context context = new Context();
				context.setVariable("usuario", usuarioBE.getUsua_Nombres() + " " + usuarioBE.getUsua_Apellidos());
				context.setVariable("alias", usuarioBE.getUsua_Alias());
				context.setVariable("clave", clave);

				EmailService.enviarMail(usuario.getUsua_Correo(), "Recuperar contraseña de la web de Parqueo",
						"CorreoRecuperarContraseniaUsuario", context);
				response.setResultado(true);

			} else {
				response.setCodError(1);
				response.setMensaje("El correo del usuario no existe");
				response.setEstado(false);
			}

		} catch (Exception e) {
			response.setCodError(0);
			response.setMensaje("Error no controlado" + e);
			response.setEstado(false);
		}

		return ResponseEntity.ok(response);
	}

	@PostMapping("/InsertarUsuario")
	public ResponseEntity<ResponseInsertarUsuario> insertarUsuario(@RequestBody Usuario usuario) {
		ResponseInsertarUsuario response = new ResponseInsertarUsuario();
		UsuarioValidator usuarioValidator = new UsuarioValidator();

		response = usuarioValidator.InsertarUsuario(usuario);

		String nombres = usuario.getUsua_Nombres();
		String apellidos = usuario.getUsua_Apellidos();

		usuario.setUsua_Nombres(formatoNombre(nombres));
		usuario.setUsua_Apellidos(formatoNombre(apellidos));

		if (response.getEstado() == true) {

			try {
				Integer temp = this.usuarioService.validarAliasUsuario(usuario.getUsua_Alias());

				if (temp == 0) {

					Integer temp1 = this.usuarioService.validarCorreoUsuario(0, usuario.getUsua_Correo());

					if (temp1 == 0) {

						String clave = GenerarPassword.getRandomPassword(8);
						usuario.setUsua_Clave(clave);

						this.usuarioService.insertarUsuario(usuario);

						// variables
						Context context = new Context();
						context.setVariable("usuario", usuario.getUsua_Nombres() + " " + usuario.getUsua_Apellidos());
						context.setVariable("alias", usuario.getUsua_Alias());
						context.setVariable("clave", clave);

						EmailService.enviarMail(usuario.getUsua_Correo(), "Acceso a la web de Parqueo",
								"CorreoRegistroUsuario", context);
						response.setResultado(true);
					} else {
						response.setCodError(3);
						response.setMensaje("El correo ya existe");
						response.setEstado(false);
					}

				} else {
					response.setCodError(2);
					response.setMensaje("El alias ya existe");
					response.setEstado(false);
				}

			} catch (Exception e) {
				response.setCodError(0);
				response.setMensaje("Error no controlado");
				response.setEstado(false);
			}
		}

		return ResponseEntity.ok(response);
	}

	@PutMapping("/ActualizarUsuario")
	public ResponseEntity<ResponseActualizarUsuario> actualizarUsuario(@RequestBody Usuario usuario) {
		ResponseActualizarUsuario response = new ResponseActualizarUsuario();
		response.setResultado(false);
		response.setEstado(true);

		String nombres = usuario.getUsua_Nombres();
		String apellidos = usuario.getUsua_Apellidos();

		usuario.setUsua_Nombres(formatoNombre(nombres));
		usuario.setUsua_Apellidos(formatoNombre(apellidos));

		try {

			Integer temp = this.usuarioService.validarCorreoUsuario(usuario.getUsua_Id(), usuario.getUsua_Correo());

			if (temp == 0) {
				this.usuarioService.actualizarUsuario(usuario);
				response.setResultado(true);
			} else {
				response.setCodError(1);
				response.setMensaje("El correo ya existe");
				response.setEstado(false);
			}

		} catch (Exception e) {
			response.setCodError(0);
			response.setMensaje("Error no controlador");
			response.setEstado(false);

		}

		return ResponseEntity.ok(response);
	}

	@GetMapping("/SeleccionarUsuario/{idusuario}")
	public ResponseEntity<ResponseSeleccionarUsuario> seleccionarUsuario(@PathVariable("idusuario") Integer idusuario) {
		ResponseSeleccionarUsuario response = new ResponseSeleccionarUsuario();
		response.setEstado(true);

		try {

			Integer temp = this.usuarioService.validarUsuarioExistePorId(idusuario);

			if (temp == 1) {
				Usuario user = this.usuarioService.seleccionarUsuario(idusuario);
				user.setUsua_Clave("");
				response.setUsuario(user);
			} else {
				response.setCodError(1);
				response.setMensaje("Usuario no registrado");
				response.setEstado(false);
			}

		} catch (Exception e) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
		}
		return ResponseEntity.ok(response);
	}

	@DeleteMapping("/EliminarUsuario/{idusuario}")
	public ResponseEntity<ResponseEliminarUsuario> eliminarUsuario(@PathVariable("idusuario") Integer idusuario) {
		ResponseEliminarUsuario response = new ResponseEliminarUsuario();
		response.setEstado(true);

		try {

			Integer temp = this.usuarioService.validarUsuarioExistePorId(idusuario);

			if (temp == 1) {
				this.usuarioService.eliminarUsuario(idusuario);
				response.setResultado(true);
			} else {
				response.setCodError(1);
				response.setMensaje("Usuario no registrado");
				response.setEstado(false);
			}

		} catch (Exception e) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
		}
		return ResponseEntity.ok(response);
	}

	@GetMapping("/ListarUsuarios/{pagenumber}/{pagesize}/{nombre}/{idperfil}/{activo}/{indicadorNombre}")
	public ResponseEntity<ResponseListarUsuarios> listarUsuarios(@PathVariable("pagenumber") Integer pagenumber,
			@PathVariable("pagesize") Integer pagesize, @PathVariable("nombre") String nombre,
			@PathVariable("idperfil") Integer idperfil, @PathVariable("activo") Integer activo,
			@PathVariable("indicadorNombre") Integer indicadorNombre) {
		ResponseListarUsuarios response = new ResponseListarUsuarios();
		response.setEstado(true);

		if(indicadorNombre==0) {
			nombre = "";
		}
		try {
			response.setLista(this.usuarioService.listarUsuarios(pagenumber, pagesize, nombre, idperfil, activo));
		} catch (Exception e) {
			response.setCodError(0);
			response.setEstado(false);
			response.setMensaje(e.getMessage());
		}

		return ResponseEntity.ok(response);
	}
	
	@GetMapping("/ReenviarCredencialesUsuario/{idusuario}")
	public ResponseEntity<ResponseRecuperarContraseniaAdmin> recuperarContraseniaAdmin(
			@PathVariable("idusuario") Integer idusuario) {
		ResponseRecuperarContraseniaAdmin response = new ResponseRecuperarContraseniaAdmin();
		response.setEstado(true);
		response.setResultado(false);

		try {

			Integer temp = this.usuarioService.validarUsuarioExistePorId(idusuario);

			if (temp == 1) {

				Usuario usuario = this.usuarioService.seleccionarUsuario(idusuario);

				// variables
				Context context = new Context();
				context.setVariable("usuario", usuario.getUsua_Nombres() + " " + usuario.getUsua_Apellidos());
				context.setVariable("alias", usuario.getUsua_Alias());
				context.setVariable("clave", usuario.getUsua_Clave());

				EmailService.enviarMail(usuario.getUsua_Correo(), "Acceso a la web de Parqueo",
						"CorreoRegistroUsuario", context);

				response.setResultado(true);

			} else {
				response.setCodError(1);
				response.setMensaje("Usuario no registrado");
				response.setEstado(false);
			}

		} catch (Exception e) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
		}
		return ResponseEntity.ok(response);
	}
	

	@GetMapping("/SeleccionarUsuarioCorreo/{correo}")
	public ResponseEntity<ResponseSeleccionarUsuario> seleccionarUsuarioCorro(@PathVariable("correo") String correo) {
		ResponseSeleccionarUsuario response = new ResponseSeleccionarUsuario();
		response.setEstado(true);
		
		try {
			
			Integer temp = this.usuarioService.validarCorreoUsuario(0, correo);
			
			if(temp==0) {
				response.setCodError(1);
				response.setMensaje("Correo no registrado");
				response.setEstado(false);
			}else {	
				Usuario usuario = this.usuarioService.seleccionarUsuarioPorCorreo(correo);	
				response.setUsuario(usuario);
			}

		} catch (Exception e) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
		}
		return ResponseEntity.ok(response);
	}

	public String formatoNombre(String dato) {
		dato = dato.toLowerCase();
		char[] arr = dato.trim().toCharArray();
		
		for(int x=0;x<dato.length();x++) {
			if(x==0) {
				arr[x] = Character.toUpperCase(arr[x]);
			}
			
			if(arr[x] == ' ') {
				if(x+1 < dato.length()) {
					if(arr[x+1] != ' ') {
						arr[x+1] = Character.toUpperCase(arr[x+1]);
					}
				}
			}
		}
		return new String(arr);
	}
}
